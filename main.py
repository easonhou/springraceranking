# -*- coding: GB2312 -*-
import datetime

INPUT_FILE = 'd:/springraceranking/input.csv'

def _parseStringToDatatime(text):
	date, time = text.split()
	date = map(int, date.split('-'))
	time = map(int, time.split(':'))
	return datetime.datetime(date[0], date[1], date[2], time[0], time[1], time[2], )

def _parseStringToSecond(text):
	mi, sec = map(int, text.replace('"', '').split("'"))
	return mi * 60 + sec

def _parsePeaceToString(sec):
	return '%2d\'%2d"' % (sec / 60, sec % 60)


class RunRecord(object):
	def __init__(self, textList):
		self.endTime    = _parseStringToDatatime(textList[0])
		self.status    = textList[1]
		self.userID    = int(textList[2])
		self.userName    = textList[3]
		self.gender    = textList[4]
		self.distance    = float(textList[5])
		self.totalTime = textList[6]
		self.runType    = textList[7]
		self.pace = _parseStringToSecond(textList[8])
		self.stepfrequency   = int(textList[9])
		self.stepLength   = float(textList[10])
	
	def getInvaildReason(self):
		if self.distance < 2.0:
			return u'距离(%.2f)少于2km' % self.distance
		# if self.stepfrequency < 120:
		# 	return u'步频(%d)少于120' % self.stepfrequency
		if not (60 * 4 <= self.pace <= 60 * 10):
			return u'配速(%s)应该在4到10分钟之间' % _parsePeaceToString(self.pace)
		return ''
	
	def getString(self):
		text = ''
		for name, des, func in [('endTime', u'完成时间', None),
								('distance', u'距离', None),
								('totalTime', u'用时', None),
								('runType', u'类型', None),
								('pace', u'配速', _parsePeaceToString),
								('stepfrequency', u'步频', None),]:
			value = getattr(self, name)
			if func:
				value = func(value)
			text += '%s: %s\t' % (des, value)
		return text



class Person(object):
	def __init__(self, id):
		self.userID = id
		self.runRecordList = []
		self.userNameList = []
		self.userName = ''
	
	def addRecord(self, runRecord):
		if runRecord.userID != self.userID:
			return
		if not self.userName :
			self.userName = runRecord.userName
			try:
				self.userName.encode('gb2312')
			except:
				self.userName = str(self.userID)
				print 'error encode', self.userID
		else:
			self.userNameList.append(runRecord.userName)
		self.runRecordList.append(runRecord)
	
	def getVaildDistance(self):
		totalDistance = 0
		for record in self.runRecordList:
			if not record.getInvaildReason():
				totalDistance += record.distance
		return totalDistance
	
	def getVaildOutdoorDistance(self):
		totalDistance = 0
		for record in self.runRecordList:
			if not record.getInvaildReason() and record.runType == u'室外跑步':
				totalDistance += record.distance
		return totalDistance
	
	def getKMTime(self, km):
		self.runRecordList.sort(key = lambda x:x.endTime)
		totalDistance = 0
		for record in self.runRecordList:
			if not record.getInvaildReason():
				totalDistance += record.distance
				if totalDistance >= km:
					return record.endTime
		return None
	
	def getString(self):
		sixtyKMTime = self.getKMTime(60)
		sixtyKMTimeText = str(sixtyKMTime) if sixtyKMTime else u'未达成'
		text = u'ID: %s\n名字: %s\n\n有效跑量:%s\n\n有效室外跑量:%s\n达成60KM时间:%s\n' % (self.userID, self.userName, self.getVaildDistance(), self.getVaildOutdoorDistance(), sixtyKMTimeText)
		vaildText = u'有效跑量:\n'
		invaildText = u'无效跑量:\n'
		for record in self.runRecordList:
			reason = record.getInvaildReason()
			if reason:
				invaildText += u'%s 原因: %s\n' % (record.getString(), reason)
			else:
				vaildText += u'%s\n' % (record.getString(),)
		return text + vaildText + invaildText


class Group(object):
	def __init__(self):
		self.personDict = {}
	
	def addRunRecordList(self, runRecordList):
		for record in runRecordList:
			if record.userID not in self.personDict:
				self.personDict[record.userID] = Person(record.userID)
			self.personDict[record.userID].addRecord(record)
	
	def getDistanceRankString(self):
		data = [(person.userName, person.getVaildDistance())for person in self.personDict.itervalues()]
		data.sort(key = lambda x:-x[1])
		text = ''
		for rank, data in enumerate(data):
			text += 'No.%s: %s %.2fKM\n' % (rank + 1, data[0], data[1])
		return text
	
	def getOutdoorDistanceRankString(self):
		data = [(person.userName, person.getVaildOutdoorDistance())for person in self.personDict.itervalues()]
		data.sort(key = lambda x:-x[1])
		text = ''
		for rank, data in enumerate(data):
			text += 'No.%s: %s %.2fKM\n' % (rank + 1, data[0], data[1])
		return text

	
	def getReachKMRankString(self, km):
		dataList = []
		for person in self.personDict.itervalues():
			endTime = person.getKMTime(km)
			if endTime:
				dataList.append((person.userName, endTime))
		dataList.sort(key = lambda x: x[1])
		text = ''
		for rank, data in enumerate(dataList):
			text += 'No.%s: %s %s\n' % (rank + 1, data[0], data[1])
		return text
	
	def produceResultFile(self):
		with open('total.txt', 'w') as f:
			f.write('室外跑步:\n')
			f.write(self.getOutdoorDistanceRankString().encode('gb2312'))
			for km in [20, 30, 40, 60]:
				f.write('\n\n%sKM达成时间排行:\n' % km)
				f.write(self.getReachKMRankString(km).encode('gb2312') + '\n')
		
		for person in self.personDict.itervalues():
			with open('%s.txt' % person.userName, 'w') as f:
				f.write(person.getString().encode('gb2312'))





import chardet
def getAllRunRecord():
	dataList = []
	with open(INPUT_FILE) as f:
		for idx, data in enumerate(f):
			if idx <= 7:
				continue
			encoding = chardet.detect(data)['encoding']
			try:
				data = data.decode(encoding)
			except:
				print 'encode error', idx, encoding
				continue
			data = data.replace('\n', '').split(',')
			record = RunRecord(data)
			dataList.append(record)
	return dataList



def main():
	group = Group()
	group.addRunRecordList(getAllRunRecord())
	group.produceResultFile()

if __name__ == "__main__":
	main()
